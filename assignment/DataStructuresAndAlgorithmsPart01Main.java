/*
	Data Structures & Algorithms: Assignment - Array Sorting and Hashmaps
	Study.com - CS 201
	Date: 12-Nov-2021

	Compiled with OpenJDK 11, Debian Linux 10 (Buster)
	Output of Java version: `java --version`

	`
	openjdk 11.0.12 2021-07-20
	OpenJDK Runtime Environment (build 11.0.12+7-post-Debian-2)
	OpenJDK 64-Bit Server VM (build 11.0.12+7-post-Debian-2, mixed mode, sharing)
	`

	Part 01

*/

import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;
import java.util.Arrays;

class DataStructuresAndAlgorithmsPart01Main {
	public static String [][] getStatesAndCapitals () {
		String [][] list = {
			{"Alabama", "Montgomery"},
			{"Alaska", "Juneau"},
			{"Arizona", "Phoenix"},
			{"Arkansas", "Little Rock"},
			{"California", "Sacramento"},
			{"Colorado", "Denver"},
			{"Connecticut", "Hartford"},
			{"Delaware", "Dover"},
			{"Florida", "Tallahassee"},
			{"Georgia", "Atlanta"},
			{"Hawaii", "Honolulu"},
			{"Idaho", "Boise"},
			{"Illinois", "Springfield"},
			{"Indiana", "Indianapolis"},
			{"Iowa", "Des Moines"},
			{"Kansas", "Topeka"},
			{"Kentucky", "Frankfort"},
			{"Louisiana", "Baton Rouge"},
			{"Maine", "Augusta"},
			{"Maryland", "Annapolis"},
			{"Massachusetts", "Boston"},
			{"Michigan", "Lansing"},
			{"Minnesota", "Saint Paul"},
			{"Mississippi", "Jackson"},
			{"Missouri", "Jefferson City"},
			{"Montana", "Helena"},
			{"Nebraska", "Lincoln"},
			{"Nevada", "Carson City"},
			{"New Hampshire", "Concord"},
			{"New Jersey", "Trenton"},
			{"New Mexico", "Santa Fe"},
			{"New York", "Albany"},
			{"North Carolina", "Raleigh"},
			{"North Dakota", "Bismarck"},
			{"Ohio", "Columbus"},
			{"Oklahoma", "Oklahoma City"},
			{"Oregon", "Salem"},
			{"Pennsylvania", "Harrisburg"},
			{"Rhode Island", "Providence"},
			{"South Carolina", "Columbia"},
			{"South Dakota", "Pierre"},
			{"Tennessee", "Nashville"},
			{"Texas", "Austin"},
			{"Utah", "Salt Lake City"},
			{"Vermont", "Montpelier"},
			{"Virginia", "Richmond"},
			{"Washington", "Olympia"},
			{"West Virginia", "Charleston"},
			{"Wisconsin", "Madison"},
			{"Wyoming", "Cheyenne"},
		};

		return list;
	}

	public static void bubbleSort (String [][] s) {
		int n = s.length;

		// from the beginning to the end (0 to 48), length - 1, because the current index and the next are compared, 49 + 1 would be out of bounds
		for (int i = 0; i < n - 1; i++) {

			// Go to the next-to-last element
			for (int j = 0; j < n - i - 1; j++) {

				// Compare strings using java "compareTo()" method - Returns -1 if string is after Ex: "Z" > "A". Returns 0 if equal "B" = "B". Returns 1 if before "C" < "D".
				// If j string is "greater than" j + 1, then change places

				if (s [j][1].compareTo (s [j + 1][1]) > 0) {
					// multidimensional array, must have two temp variables
					String tempState = s [j][0];
					String tempCapital = s [j][1];

					// swap state
					s [j][0] = s [j+1][0];

					// swap captial
					s [j][1] = s [j+1][1];

					// swap temp variables, state and capital
					s [j+1][0] = tempState;
					s [j+1][1] = tempCapital;
				}
			}
		}

	}

	public static void main (String [] args) {
		String [][] list = getStatesAndCapitals();

		// Pick a random state and ask the user for the capital

		// Get a random number from 0 - 49
		int randomNum = ThreadLocalRandom.current().nextInt (0, 50);
		System.out.printf("State capital guessing game\n");
		System.out.printf("---------------------------\n");
		System.out.printf("State: %s\n", list[randomNum][0]);

		// get user input for state capital

		// TODO: remove - For debugging
		// System.out.println(list [randomNum][1].toLowerCase ());

		System.out.printf ("Enter state capital: ");
		Scanner scanner = new Scanner (System.in);
		String capital = scanner.nextLine();  // Read user input

		System.out.printf ("You typed: '%s'\n", capital.toLowerCase());

		if (list [randomNum][1].toLowerCase ().equals (capital.toLowerCase())) {
			System.out.printf ("Correct!\n");
		} else {
			System.out.printf ("Incorrect!\nThe correct answer was: '%s'\n", list [randomNum][1]);
		}

		System.out.println ("Here are all of the United States and their capitals, sorted by state\n");

		String fixedSizeFormat = "%-25s %-15s";
		for (int i = 0; i < list.length; i++) {
			String s1 = String.format ("State: %s", list[i][0]);
			String s2 = String.format ("Capital: %s\n", list[i][1]);
			System.out.format (fixedSizeFormat, s1, s2);
		}

		System.out.println("\nBubble sorting multidimensional array by capital...");

		// resort array
		bubbleSort(list);

		// display updated list
		System.out.println ("Updated list of the United States and their capitals, sorted by capital\n");

		for (int i = 0; i < list.length; i++) {
			String s1 = String.format ("State: %s", list[i][0]);
			String s2 = String.format ("Capital: %s\n", list[i][1]);
			System.out.format (fixedSizeFormat, s1, s2);
		}

		System.out.printf ("Enter as many state capitals as you wish, separated by a comma (Ex: 'one,two,three...'): ");
		String listOfCapitals = scanner.nextLine();  // Read user input

		String [] userInput = listOfCapitals.split(",");
		int totalCorrect = 0;

		for (int i = 0; i < list.length; i++) {
			for (int j = 0; j < userInput.length; j++) {
				if (list [i][1].toLowerCase ().equals (userInput[j].toLowerCase())) {
					totalCorrect += 1;
				}
			}
		}

		System.out.printf("You got %d capitals correct!\n", totalCorrect); 
	}
}
