/*
	Data Structures & Algorithms: Assignment - Array Sorting and Hashmaps
	Study.com - CS 201
	Date: 12-Nov-2021

	Part 02

	Compiled with OpenJDK 11, Debian Linux 10 (Buster)
	Output of Java version: `java --version`

	`
	openjdk 11.0.12 2021-07-20
	OpenJDK Runtime Environment (build 11.0.12+7-post-Debian-2)
	OpenJDK 64-Bit Server VM (build 11.0.12+7-post-Debian-2, mixed mode, sharing)
	`

	I hope that I did this correctly. I was a little confused by the instructions.

*/

import java.util.HashMap;
import java.util.Scanner;

/*
	Uses a binary search tree for storing State and Capital strings.
*/
class CustomTreeMap {
	Node root;

	public CustomTreeMap() {
		root = null;
	}

	public void printTree () {
		System.out.printf("Traversing In Order: \n");
		printInOrder(root);
		System.out.printf("\n");

		/*
		System.out.printf("Traversing Pre Order: "); 
		printPreOrder(root);
		System.out.printf("\n");

		System.out.printf("Traversing Post Order: "); 
		printPostOrder(root);
		System.out.printf("\n");
		*/
	}

	// Different ways of traversing a binary search tree: pre-order, post-order, in-order

	// root, left, right
	public void printPreOrder (Node node) {
		if (node == null) {
			return;
		}

		System.out.printf("%s", node.toString ());
		printInOrder(node.leftChild);
		printInOrder(node.rightChild);
	}

	// left, right, root
	public void printPostOrder (Node node) {
		if (node == null) {
			return;
		}

		printInOrder(node.leftChild);
		printInOrder(node.rightChild);
		System.out.printf("%s", node.toString ());
	}

	// left, root, right
	public void printInOrder (Node node) {
		if (node == null) {
			return;
		}

		printInOrder(node.leftChild);
		System.out.printf("%s", node.toString ());
		printInOrder(node.rightChild);
	}

	// Returns the parent node of the leftmost node available
	public Node traverseInOrder(Node node) {
		if (node.leftChild != null) {
			if (node.leftChild.leftChild != null) {
				traverseInOrder (node.leftChild);
			}
			if (node.leftChild.leftChild == null) {
				return node;
			}
		}

		return node;
	}

	//Accessible insert method to call the recursive one
	public void insert (String state, String capital) {
		root = insertNode (root, state, capital);
	}


	// Recursive insert
	public static Node insertNode (Node node, String state, String capital) {
		if (node == null) {
			node = new Node (state, capital);
			return node;
		}

		// sort by state and then capital
		if (state.compareTo (node.getState()) <= 0) {
			node.leftChild = insertNode(node.leftChild, state, capital);
		} else if (state.compareTo (node.getState()) > 0) {
			node.rightChild = insertNode(node.rightChild, state, capital);
		}

		return node;
	}

	// Accessible find method to call the recursive one
	public Node find (String state) {
		Node node = findNode (root, state);
		return node;
	}

	// Recursive search
	public static Node findNode (Node node, String state) {
		if (node == null)
			return null;

		state = state.toLowerCase ();

		// IMPORTANT: the comparison is case-insensitive
		if (state.equals (node.getState ().toLowerCase ())) {
			return node;
		}

		if (state.compareTo (node.getState ().toLowerCase ()) <= 0) {
			return findNode (node.leftChild, state);
		} else if (state.compareTo (node.getState ().toLowerCase ()) > 0) {
			return findNode (node.rightChild, state);
		}

		return null;
	}
}


// Binary Search Tree Node
class Node {
	
	private String _capital;
	private String  _state;

	public Node leftChild;
	public Node rightChild;

	// Constructor, error check strings
	public Node (String state, String capital) {
		if (state == null) {
			state = "";
		}

		if (capital == null) {
			capital = "";
		}

		this._state = state;
		this._capital = capital;
		leftChild = rightChild = null;
	}

	public String getCapital () {
		return _capital;
	}

	public String getState () {
		return _state;
	}

	public String toString () {
		return String.format ("State: %s Capital: %s\n", this._state, this._capital);
	}
}

class DataStructuresAndAlgorithmsPart02Main {
	public static HashMap <String, String> getHashMap () {
		HashMap <String, String> statesAndCapitals = new HashMap <String, String> ();

		statesAndCapitals.put ("Alabama", "Montgomery");
		statesAndCapitals.put ("Alaska", "Juneau");
		statesAndCapitals.put ("Arizona", "Phoenix");
		statesAndCapitals.put ("Arkansas", "Little Rock");
		statesAndCapitals.put ("California", "Sacramento");
		statesAndCapitals.put ("Colorado", "Denver");
		statesAndCapitals.put ("Connecticut", "Hartford");
		statesAndCapitals.put ("Delaware", "Dover");
		statesAndCapitals.put ("Florida", "Tallahassee");
		statesAndCapitals.put ("Georgia", "Atlanta");
		statesAndCapitals.put ("Hawaii", "Honolulu");
		statesAndCapitals.put ("Idaho", "Boise");
		statesAndCapitals.put ("Illinois", "Springfield");
		statesAndCapitals.put ("Indiana", "Indianapolis");
		statesAndCapitals.put ("Iowa", "Des Moines");
		statesAndCapitals.put ("Kansas", "Topeka");
		statesAndCapitals.put ("Kentucky", "Frankfort");
		statesAndCapitals.put ("Louisiana", "Baton Rouge");
		statesAndCapitals.put ("Maine", "Augusta");
		statesAndCapitals.put ("Maryland", "Annapolis");
		statesAndCapitals.put ("Massachusetts", "Boston");
		statesAndCapitals.put ("Michigan", "Lansing");
		statesAndCapitals.put ("Minnesota", "Saint Paul");
		statesAndCapitals.put ("Mississippi", "Jackson");
		statesAndCapitals.put ("Missouri", "Jefferson City");
		statesAndCapitals.put ("Montana", "Helena");
		statesAndCapitals.put ("Nebraska", "Lincoln");
		statesAndCapitals.put ("Nevada", "Carson City");
		statesAndCapitals.put ("New Hampshire", "Concord");
		statesAndCapitals.put ("New Jersey", "Trenton");
		statesAndCapitals.put ("New Mexico", "Santa Fe");
		statesAndCapitals.put ("New York", "Albany");
		statesAndCapitals.put ("North Carolina", "Raleigh");
		statesAndCapitals.put ("North Dakota", "Bismarck");
		statesAndCapitals.put ("Ohio", "Columbus");
		statesAndCapitals.put ("Oklahoma", "Oklahoma City");
		statesAndCapitals.put ("Oregon", "Salem");
		statesAndCapitals.put ("Pennsylvania", "Harrisburg");
		statesAndCapitals.put ("Rhode Island", "Providence");
		statesAndCapitals.put ("South Carolina", "Columbia");
		statesAndCapitals.put ("South Dakota", "Pierre");
		statesAndCapitals.put ("Tennessee", "Nashville");
		statesAndCapitals.put ("Texas", "Austin");
		statesAndCapitals.put ("Utah", "Salt Lake City");
		statesAndCapitals.put ("Vermont", "Montpelier");
		statesAndCapitals.put ("Virginia", "Richmond");
		statesAndCapitals.put ("Washington", "Olympia");
		statesAndCapitals.put ("West Virginia", "Charleston");
		statesAndCapitals.put ("Wisconsin", "Madison");
		statesAndCapitals.put ("Wyoming", "Cheyenne");

		return statesAndCapitals;
	}

	public static void main (String [] args) {

		System.out.println ("Here are all of the United States and their capitals, stored in a HashMap. They are unordered.\n");

		// Create an unordered HashMap
		HashMap <String, String> hm = getHashMap();

		// Loop through HashMap, print keys and values
		// String formatting is to make the console output look nice.
		String fixedSizeFormat = "%-25s %-15s";
		for (String stateString : hm.keySet()) {
			String s1 = String.format ("State: %s", stateString);
			String s2 = String.format ("Capital: %s\n", hm.get(stateString));
			System.out.format (fixedSizeFormat, s1, s2);
		}

		System.out.println ("\nCreating a CustomTreeMap type to sort states alphabetically...\n");

		CustomTreeMap tree = new CustomTreeMap ();

		// loop through the hashmap, switch the key and value (capital becomes the key, and the state becomes the value)
		// the data is sorted as it is added to the CustomTreeMap / Binary Search Tree (BST) using Node.insertNode() and the String.compareTo () methods
		for (String key : hm.keySet()) {
			tree.insert(key, hm.get (key));
		}

		// TODO: remove - For debugging purposes
		tree.printTree();

		System.out.printf ("Enter a state: ");
		Scanner scanner = new Scanner (System.in);
		String stateString = scanner.nextLine();  // Read user input

		System.out.printf ("You typed: '%s'\n", stateString);

		// the find method is case-insensitive
		Node n = tree.find (stateString);

		if (n != null) {
			System.out.printf ("State found! The capital is: %s\n", n.getCapital() );
		} else {
			System.out.printf ("State not found!\n");
		}

	}
}
