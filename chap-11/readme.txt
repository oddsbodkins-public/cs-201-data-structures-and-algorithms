Binary Search Tree
-Add elements
-Search
-Delete elements (not in tree, no children [leaf], one child, two children [replace])

AVL Trees, Adelson Velsky Landis Tree
-Self-balancing tree, insertion or deletion makes tree unbalanced
-Balance factor, difference in height between left and right child node with respect to a particular node
-Rotation, move child node left or right
-Used in frequent data lookups

Splay tree
-Rotates
-Self-balancing

Red and black trees
-Self-balancing
-Colors must match

Multiway search (more than 2 children)
-2,3,4 search
-Root has 1,2,3 nodes
-Children have 2,3,4 nodes
