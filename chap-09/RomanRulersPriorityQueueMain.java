import java.lang.Comparable;
import java.util.PriorityQueue;

class RomanRulersPriorityQueueMain {
	public static void main (String [] args) {

		PriorityQueue <RomanRulers> theRulers = new PriorityQueue<> ();

		RomanRulers Augustus = new RomanRulers(1, "Augustus");
		RomanRulers Tiberius = new RomanRulers(2, "Tiberius");
		RomanRulers Caligula = new RomanRulers(3, "Caligula");
		RomanRulers Claudius = new RomanRulers(4, "Claudius");
		RomanRulers Nero = new RomanRulers(5, "Nero");
		RomanRulers Galba = new RomanRulers(6, "Galba");
		RomanRulers Otho = new RomanRulers(7, "Otho");
		RomanRulers AulusVitellius = new RomanRulers(8, "Aulus Vitellius");
		RomanRulers Vespasian = new RomanRulers(9, "Vespasian");
		RomanRulers Titus = new RomanRulers(10, "Titus");
		RomanRulers Domitian = new RomanRulers(11, "Domitian");
		RomanRulers Nerva = new RomanRulers(12, "Nerva");

		theRulers.add(Augustus);
		theRulers.add(AulusVitellius);
		theRulers.add(Caligula);
		theRulers.add(Claudius);
		theRulers.add(Domitian);
		theRulers.add(Galba);
		theRulers.add(Nero);
		theRulers.add(Nerva);
		theRulers.add(Otho);
		theRulers.add(Tiberius);
		theRulers.add(Titus);
		theRulers.add(Vespasian);

		while (!theRulers.isEmpty()) {
			System.out.println("Rulers Deceased: " + theRulers.remove());
		}
	}
}

class RomanRulers implements Comparable<RomanRulers> {
	private int rulerID;
	private String rulerName;

	public RomanRulers (int id, String name) {
		rulerID = id;
		rulerName = name;
	}
	public int getRulerID() {
		return rulerID;
	}
	public String getRulerName() {
		return rulerName;
	}

	public boolean equals (RomanRulers theOther) {
		return this.getRulerID() == theOther.getRulerID();
	}

	//@Override
	public int compareTo (RomanRulers theOther) {
		if (this.equals (theOther))
			return 0;
		else if (getRulerID () > theOther.getRulerID ())
			return 1;
		else
			return -1;
	}

	public String toString() {
		return String.format ("Ruler Succession #%d\tRuler Name: %s", getRulerID(), getRulerName());
	}
}
