import java.lang.Comparable;
import java.util.PriorityQueue;

class EmergencyRoomPriorityQueueMain {
	public static void main (String [] args) {
		Patient er1 = new Patient(4, "Vinnie Pinatino", "Broken Nose");
		Patient er2 = new Patient(1, "Gabe Scholamachia", "Chest Pain");
		Patient er3 = new Patient(3, "Margaret Sinpliny", "Broken Leg");
		Patient er4 = new Patient(5, "Walter Comettenin", "Broken Finger");
		Patient er5 = new Patient(2, "Cynthia Bittonnia", "Shortness of Breath");

		PriorityQueue <Patient> theER = new PriorityQueue();
		theER.add(er1);
		theER.add(er2);
		theER.add(er3);
		theER.add(er4);
		theER.add(er5);

		for (Patient p: theER) {
			System.out.println(p);
		}

		theER.remove();
		System.out.printf("There are %s patients in the queue.\n", theER.size());
	}
}

class Patient implements Comparable<Patient>{
	private int severityID;
	private String patientName;
	private String conditionName;
	
	public Patient (int severity, String name, String condition) {
		severityID = severity;
		patientName = name;
		conditionName = condition;
	}

	public int getSeverityID() { return severityID; }
	public String getPatientName() { return patientName; }
	public String getConditionName() { return conditionName; }

	public boolean equals(Patient theOtherPatient) {
		return this.getSeverityID() == theOtherPatient.getSeverityID();
	}

	//	@Override
	public int compareTo (Patient theOtherPatient) {
		if (this.equals(theOtherPatient))
		return 0;
		else if (getSeverityID() > theOtherPatient.getSeverityID())
		return 1;
		else
		return -1;
	}

	public String toString () {
		String s;
		s = String.format("Next Patient (%d):\t%s\tCondition:%s", getSeverityID(), getPatientName(), getConditionName());
		return s;
	}
}
