import java.util.Arrays;

/*
	Performance is O(N^2) 
*/
class InsertionSortMain {

	public static void insertionSort (int sortArray []) {
		//start looping through the array

		for(int i = 1; i < sortArray.length; i++) {

			//start again in the array, this time
			//decrementing by 1, until you've reached the beginning
			//and the next element is greater than the current

			for(int j = i; (j > 0 && sortArray[j-1] > sortArray[j]); j--) {
				//save a temporary bucket (the ? from the graphic)
				//re-sort elements so the next is greater than current
				int temp = sortArray[j];
				sortArray[j] = sortArray[j-1];
				sortArray[j-1] = temp;
			}
		}
	}

	public static void main (String [] args) {
		int [] steps = {45, 22, 1, 7, 9, 11, 4, 2};
		System.out.println("Before Sort:");
		System.out.println(Arrays.toString(steps));

		//time check
		long startTime = System.nanoTime();

		//call the Insertion Sort Method
		insertionSort(steps);

		//time check
		long endTime = System.nanoTime();
		long totalTime = endTime - startTime;

		System.out.println("After Sort:");
		System.out.println(Arrays.toString(steps));

		System.out.println("Time taken " + totalTime + " ns");
	}
}
