import java.util.Arrays;


/*
	Time complexity is O(n^2), two for loops

	Useful in a linked list where the addition or removal of an element from a list is required

*/
class SelectionSortMain {
	public static void sort (int arr[])
	{
		int n = arr.length;

		// Traverse the unsorted subarray
		for (int i = 0; i < n-1; i++)
		{
			// Find the smallest element in unsorted array
			int min = i;

			for (int j = i+1; j < n; j++) {
				if (arr[j] < arr[min])
					min = j;
			}

			// Swap the smallest element with the first element
			int temp = arr[min];
			arr[min] = arr[i];
			arr[i] = temp;
		}
	}


	public static void main (String [] args) {
		int arr[] = {95, 42, 13, 9, 23};
		sort(arr);
		System.out.println("The sorted array is: ");
		System.out.println(Arrays.toString(arr));
	}
}
