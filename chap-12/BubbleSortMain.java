import java.util.Arrays;

/*
	performance is O(n^2)

	Which means the sort time increases as the length of the array increases by a power of two
*/
class BubbleSortClass {
	public BubbleSortClass() {
	}

	public void bubbleSort(int mySimpleArray[]) {

		int n = mySimpleArray.length;

		//first loop traverses to the end of the list
		//remember arrays start at 0!
		for (int i = 0; i < n-1; i++) {

			//next, go to the next-to-last element
			for (int j = 0; j < n-i-1; j++) {

				//compare.
				//if j (e.g., 11) is greater than j+1 (3)
				//then swap.
				if (mySimpleArray[j] > mySimpleArray[j+1]) {
					//swap mySimpleArray[j+1] and mySimpleArray[j]
					//these steps create a temporary holding variable,
					//then flip one to the other
					int temp = mySimpleArray[j];
					mySimpleArray[j] = mySimpleArray[j+1];
					mySimpleArray[j+1] = temp;
				}

				if (i == 0 && j == 0) {
					System.out.println(Arrays.toString(mySimpleArray));
				}
			}
		}
	}
}

class BubbleSortMain {
	public static void main (String [] args) {

		//setup the array
		int mySimpleArray[] = {7,4,6,1,2}; //{7, 5, 1, 19, 42, 3, 11, 22, 4, 88};

		BubbleSortClass b = new BubbleSortClass();
		b.bubbleSort(mySimpleArray);

		System.out.println("Sorted mySimpleArrayay");

		//call the print function
		System.out.println(Arrays.toString(mySimpleArray));
	}
}
