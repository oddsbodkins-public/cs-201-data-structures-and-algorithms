import java.util.Arrays;

/*

	Quick Sort has a Big O rating of O(n log n) for average performance and an O(n^2) worst case performance.

	There are variations of the Quick Sort algorithm based on how the list is partitioned. 
	Tony Hoare's original method uses two indexes that start at the ends of the list and work towards the middle comparing and swapping elements.

	Internal sorting is when sorting is done in memory, while external sorting uses files. 

*/
class QuickSortMain {
	public static void main (String [] args) {

		QuickSortClass Sort = new QuickSortClass ();

		Sort.data = new int[] {24,2,45,20,56,75,16,56,99,53,12};

		int length = Sort.data.length;

		System.out.println("Unsorted list.");
		System.out.println (Arrays.toString(Sort.data));

		Sort.quicksort (0, length-1);

		System.out.println("Sorted list.");
		System.out.println (Arrays.toString(Sort.data));

	}
}

class QuickSortClass {

	public int [] data;

	public QuickSortClass() {
		this.data = null;
	}

	private void swap (int i, int j) {
		int temp;
		temp = data[i];
		data[i] = data[j];
		data[j] = temp;
	}

	public void quicksort ( int left, int right ) {
		int i, last;

		if (left >= right) {
			return;
		}

		swap (left, (left + right) / 2);

		last = left;
		for ( i = left+1; i <= right; i++ ) { // partitioning
			if ( data[i] < data[left] ) {
				swap( ++last, i );
			}
		}

		swap ( left, last );

		quicksort ( left, last-1 );
		quicksort ( last+1, right );
	} // quicksort

}
