The Big-O Notation is a standard by which the performances of algorithmic functions are measured. In the Big-O Notation, there are 4 main different time complexities that reflect the performance of a function.

    O(1) - Constant Time Complexity
    O(n) - Linear Time Complexity
    O(log n) - Logarithmic Time Complexity
    O(n^2) - Quadratic Time complexity 

-----

Open Addressing
-Used with Hash Maps, hashing functions
-If location is already occupied, use linear probing, quadratic probing, or double hashing
- Linear probing (if collision occurs), calculate hash address, add one, if the next space is taken, add one again, repeat until open slot found. Clustering Problem occurs, data grouped together near hash position, rather than equally distributed, which decreases performance.
- Quadratic Probing, must keep track of number of hashing calculations (probe number). Alternate location = ((probe number) ^ 2 + base address) mod (array length) 
