public class CircularLinkedList {
	public int size = 0;
	public CircularLinkedListNode head = null;
	public CircularLinkedListNode tail = null;

	// print the list
	public void print () {
		String s = "The list so far: ";

		CircularLinkedListNode temp = head;

		do {
			s += " " + temp.element;
			temp = temp.next;

		} while (temp != head);

		System.out.println(s);
	}


	// add a new node to the start of the linked list
	public void addNodeToHead (int element) {
		CircularLinkedListNode n = new CircularLinkedListNode(element);

		if (size == 0){
			head = n;
			tail = n;
			n.next = head;
		} else {
			CircularLinkedListNode temp = head;
			n.next = temp;
			head = n;
			tail.next = head;
		}

		size++;

		System.out.printf("Adding Node %d to Head\n", element);
	}

	public void addNodeToTail (int element) {
		if(size == 0){
			addNodeToHead (element);
		} else {
			CircularLinkedListNode n = new CircularLinkedListNode (element);
			tail.next = n;
			tail = n;
			tail.next = head;
			size++;
		}

		System.out.printf("Node %d added to tail\n", element);
	}

	public void rotateElement() {
		System.out.println ("Rotating!");
		tail = head;
		head = head.next;
	}

	public void deleteNodeFromTail () {
		System.out.println("\nDeleting Node " + tail.element + " from Tail");

		if (tail.next == tail) {
			tail = null;
		}
		CircularLinkedListNode newTail = tail;

		while (newTail.next != tail) {
			newTail = newTail.next;
		}

		newTail.next = tail.next;
		tail = newTail;
	}
}
