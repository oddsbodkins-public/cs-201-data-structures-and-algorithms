/*
	Insertion-Sort according to Study.com page
	Left to Right Sort?
	If value on right is less than the value on the left, switch places.
*/

import java.util.Arrays;

class SortArray {
	public static void main(String [] args) {
		int [] array = {45,12,85,32,89,39,69,44,42,1,6,8};
		int temp = 0;

		for (int i = 1; i < array.length; i++) {
			for (int j = i; j > 0; j--) {
				if (array[j] < array[j - 1]) {
					temp = array[j];
					array[j] = array[j - 1];
					array[j - 1] = temp;
				}
			}
		}

		System.out.println("By hand: " + Arrays.toString(array));
		JavaUtilSort j = new JavaUtilSort();
		j.librarySort();

	}
}

class JavaUtilSort {
	public void librarySort() {
		System.out.println("\njava.util.Arrays.sort()\n-----------------------------");
		int [] array = {45,12,85,32,89,39,69,44,42,1,6,8};
		Arrays.sort(array, 4, 8);
		System.out.println("Partially sorted: " + Arrays.toString(array));
		Arrays.sort(array);
		System.out.println("Completely sorted: " + Arrays.toString(array));

		int index = Arrays.binarySearch(array, 42);
		System.out.println("Index of 42 is: " + index);
	}
}
