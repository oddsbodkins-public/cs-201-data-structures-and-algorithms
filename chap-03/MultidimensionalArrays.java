import java.lang.Math;

class MultidimensionalArrays {
	public static void main(String [] args) {
		int row, column;
		int userX = 0, userY = 0;
		double dist, shortestDistance = 0;

		int [][] gps = {
			{ -2, 4},
			{3, 2},
			{-5, -5},
			{4, -1},
		};

		for (row = 0; row < gps.length; row++) {
			for (column = 0; column < gps[row].length; column++) {
				dist = distance (gps[row][0], gps[row][1], gps[column][0], gps[column][1]);

				if (shortestDistance < dist) {
					userX = row;
					userY = column;
					shortestDistance = dist;
				}
			}
		}

		System.out.printf("The nearest point is: (%d,%d)\n", gps[userY][0], gps[userX][1]);
	}

	public static double distance (double x1, double y1, double x2, double y2) {
		return Math.sqrt( (x2 - x1) * (x2 - x1) * (y2 - y1) * (y2 - y1) );
	}
}
