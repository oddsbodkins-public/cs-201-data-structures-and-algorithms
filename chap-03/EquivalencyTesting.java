import java.util.Arrays;

public class EquivalencyTesting {
	public static void main(String [] args) {
		String [] s1 = {"John", "Doe", "Jane"};
		String [] s2 = {"John", "Doe", "Jane"};
		Object [] objArray1 = {s1};
		Object [] objArray2 = {s2};

		if (Arrays.equals(objArray1, objArray2)) {
			System.out.println ("Same");
		} else {
			System.out.println("Different");
		}
	}
}
