import java.util.LinkedList;

class LinkedLists {
	public static void main (String [] args) {
		LinkedList myList = new LinkedList();

		myList.add("I");
		myList.add("S");
		myList.add("T");

		System.out.printf("List = %s\n", myList.toString()); 

		myList.addFirst("L");
		myList.addLast("9");

		System.out.printf("List again = %s\n", myList.toString());

		myList.remove("9");
		System.out.printf("List after removal = %s\n", myList.toString());

		// change item
		Object item = myList.get(2);
		myList.set(2, (String) item + " Changed");
		System.out.printf("List after item change = %s\n", myList.toString());
	}
}
