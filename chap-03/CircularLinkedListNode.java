public class CircularLinkedListNode {
	int element;
	CircularLinkedListNode next;

	public CircularLinkedListNode(int element) {
		this.element = element;
	}
}
