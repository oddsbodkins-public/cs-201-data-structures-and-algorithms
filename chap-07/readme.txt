Dynamic Arrays

1. If an array already exists and needs to grow, create a new array with larger size, use System.arraycopy(), and assign the new array to the old array
2. Wrap the above in a class
3. Use the java.util.ArrayList class, it automatically grows with '.add(...)'
