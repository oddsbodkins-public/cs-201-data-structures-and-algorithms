import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

class JavaCollectionsMain {
	public static void main (String [] args) {

		List<String> myList = new ArrayList<>();
		myList.add("This");
		myList.add("makes");
		myList.add("no");
		myList.add("sense");

		System.out.println(myList.get(1));

		myList.set(2, "perfect");
		for (String theValue : myList) {
			System.out.print(theValue + " ");
		}

		List<String> listOfNames = new ArrayList<>();
		listOfNames.add("jimmy");
		listOfNames.add("sally");
		listOfNames.add("cindy");
		listOfNames.add("rhona");
		listOfNames.add("john");
		listOfNames.add("brenda");
		listOfNames.add("lona");

		if (listOfNames.contains("brenda")) {
			System.out.println("Record found!");
		}
		else {
			System.out.println("Record not found!");
		}

		Collections.sort(listOfNames);
		for (String theName : listOfNames) {
			System.out.println(theName);
		}

		for (int i=0; i < listOfNames.size(); i++) {
			String tempName = listOfNames.get(i);
			listOfNames.set(i, tempName.substring(0, 1).toUpperCase() + tempName.substring(1));
		}

		System.out.println(listOfNames);

	}
}
