import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

class IterationOnSetsMain {
	public static void hashSet() {
		System.out.println("Hash Set\n");

		// create our Set
		Set < String > trees = new HashSet();
		trees.add("Larch");
		trees.add("Pine");
		trees.add("Balsam");
		trees.add("Birch");
		trees.add("Ash");

		//iterate over set
		for (String s: trees) {
			System.out.println("Tree = " + s);
		}

		// new to Java 8
		trees.forEach(System.out::println);

		// using the Iterator
		Iterator < String > ir = trees.iterator();
		while (ir.hasNext()) {
			System.out.println("Iterator Result = " + ir.next());
		}

	}

	public static void treeSetStrings() {
		System.out.println("String Tree Set\n");
		//create our Set
		Set < String > trees = new TreeSet();
		trees.add("Larch");
		trees.add("Pine");
		trees.add("Balsam");
		trees.add("Birch");
		trees.add("Ash");
		Iterator < String > ir = trees.iterator();
		while (ir.hasNext()) {
			System.out.println("Iterator Result = " + ir.next());
		}
	}

	public static void treeSetDoubles() {
		System.out.println("Double Tree Set\n");

		Set < Double > scores = new TreeSet();
		scores.add(1.59343);
		scores.add(.9934847);
		scores.add(-3.3444);
		Iterator < Double > ir = scores.iterator();
		while (ir.hasNext()) {
			System.out.println("Score: " + ir.next());
		}
	}

	public static void main (String [] args) {

		// Sets are part of the java.lang.Collections class
		// HashSet is unordered
		// TreeSet maintains ascending order (Ex: 1 ... 10) 

		hashSet();
		treeSetStrings();
		treeSetDoubles();
	}
}
