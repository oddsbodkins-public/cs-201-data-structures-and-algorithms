class RecursionOrIteration {
	private static String reverseStringRecursive (String s) {
		if (s.length() == 0) {
			return "";
		} else {
			return reverseStringRecursive(s.substring(1)) + s.charAt(0);

			/*
				Walking through the recursive function

				IN: "test" OUT: rSR("est") + "t"
				IN: "est" OUT: rSR("st") + "e"
				IN: "st" OUT: rSR("t") + "s"
				IN: "t" OUT: rSR("") + "t"
				IN: "" OUT: ""
			*/
		}
	}

	private static long factorialRecursive (int n) {
		if (n < 1) { // I added this for safety, not in lesson
			return 0;
		} else if (n == 1) {
			return 1;
		} else {
			return n * factorialRecursive (n - 1);
		}
	}

	private static String reverseNameIterative (String theName) {
		StringBuilder reverseName = new StringBuilder();
		char [] strChars = theName.toCharArray();
		int len = theName.length() -1;
		for (int i = len; i >= 0; i--) {
			reverseName.append(strChars[i]);
		}
		return (reverseName.toString());
	}

	private static long factorialIterative (int theNumber) {
		int theFactorial = 1;
		for (int i = 1; i <= theNumber; i++) {
			theFactorial = theFactorial * i;
		}
		return theFactorial;
	}

	public static void main (String [] args) {
		// https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/lang/String.html#substring(int)

		String s = "t";
		System.out.println(s.substring(1));
		System.out.println(reverseStringRecursive("test"));

		System.out.println(factorialRecursive(3));

		System.out.println(reverseNameIterative("blah"));

		System.out.println(factorialIterative(3));
	}
}
