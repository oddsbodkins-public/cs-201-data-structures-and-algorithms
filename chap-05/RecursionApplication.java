class RecursionApplication {

	public static long calcGCD(int a, int b) {
		if(b != 0) {
			System.out.println(a%b);
			return calcGCD(b, a % b);
		} else {
			return a;
		}
	}

	private static long factorialRecursive (int n) {
		if (n < 1) { // I added this for safety, not in lesson
			return 0;
		} else if (n == 1) {
			return 1;
		} else {
			return n * factorialRecursive (n - 1);
		}
	}

	static int a = 1, b = 1, c = 0;

	static void printFibonacci(int count) {
		if(count > 0) {
			//c is the sum of the previous two
			c = b + a;
			//a is the new b (as we shift down the line)
			a = b;
			//b is the new a
			b = c;
			System.out.print(" " + c);
			printFibonacci(count-1);
		}
	}

	public static void main (String [] args) {
		calcGCD(10, 50);

		System.out.println("The factorial of 11 " + factorialRecursive(11));

		System.out.println("\n");
		System.out.println("***** FIBONACCI SERIES ***** \n");
		int count = 15;
		System.out.print(a + " " + b);
		//count - 2 since we already showed 2 numbers
		printFibonacci(count-2);
		System.out.println("");

		TowerOfHanoi tower = new TowerOfHanoi();
		int disks = 3;
		tower.solvePuzzle(disks, "Peg 1", "Peg 2", "Peg 3");
	}
}

class TowerOfHanoi {
	public void solvePuzzle(int n, String begin, String temp, String end) {
		if(n==1) {
			System.out.println(begin + " ---> " + end);
		} else {
			solvePuzzle(n-1, begin, end, temp);
			System.out.println(begin + " ---> " + end);
			solvePuzzle(n-1, temp, begin, end);
		}
	}
}
