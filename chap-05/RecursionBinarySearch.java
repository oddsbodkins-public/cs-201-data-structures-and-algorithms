class RecursionBinarySearch {


	// Array must be sorted! (low to high, ascending)
	public static int recursiveBinarySearch (int[] sortedArray, int begin, int end, int key) {

		if (begin < end) {
			int middle = begin + (end - begin) / 2;

			if(key < sortedArray[middle]) {
				return recursiveBinarySearch(sortedArray, begin, middle, key);
			} else if (key > sortedArray[middle]) {
				return recursiveBinarySearch(sortedArray, middle+1, end, key);
			} else {
				return middle;
			}
		}

/*
		Typo? (from lesson) Assign 1 to 'begin' and return negative value of 'begin'?

		return -(begin = 1);

		I replaced it with the below return statement
*/
		return -1;
	}

	public static void main (String [] args) {
		//establish our sorted array

		int[] sortedArr = {1, 53, 62, 133, 384, 553, 605, 897, 1035, 1234};
		int searchIndex = recursiveBinarySearch(sortedArr, 0, sortedArr.length, 606);
		System.out.println("I Found 605 at index " + searchIndex);
	}
}
