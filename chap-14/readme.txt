Graph theory

Isolated vertex - no paths to it
Adjacent vertices - vertices that are neighbors
Degree of a vertex - how many lines connect to a vertex
Even or odd degree = even or odd vertex
Multiple edges - line that connect the same vertices together
Adjacent edges - lines that are next to each other
Path = route taking you from one vertex to another
Length of the path = the number of edges that you take
Circuit = a path that takes you back to the same vertex
Loop = an edge or path that connects to the same vertex, not always allowed
Connected graph = all vertices are connected
Disconnected graph = not all vertices are connected
Subgraphs = two graphs that are not connected
Bride = connection between two subgraphs, only one path

-----

Weighted graphs
Dijkstra algorithm
