import java.lang.StringBuilder;

class TextDataStructure {
	public static void main (String [] args) {
		String stopSign = "Stop";
		stopSign.toCharArray();

		char[] stopSign2 = {'S', 'T', 'O', 'P'};
		String s7 = new String(stopSign2);

		String s1 = "STOP";
		String s2 = s1;
		s1 = "STAP";
		System.out.println(s2);
		System.out.println(s1);
		//what will s2 print?

		String sign = "STOP";
		//convert to char array
		char[] newSign = sign.toCharArray();

		newSign[2] = '!';

		//cycle through chars
		for (char c : newSign) {
			System.out.println(c);
		}

		System.out.println(sign);

		StringBuilder sb1 = new StringBuilder("STO");
		System.out.println(sb1);
		//append the P
		sb1.append("P");
		System.out.println(sb1);
		//insert an exclamation point
		sb1.insert(0, "!");
		System.out.println(sb1);
		//reverse, reverse!
		sb1.reverse();
		System.out.println(sb1);

	}
}
