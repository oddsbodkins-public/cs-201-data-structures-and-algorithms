String searching algorithms

- Brute Force
- "Peter Piper picked a peck of pickled peppers", Search string "pep"
- Loop through each character and compare to the first character of the search string, if it matches check the next until the end, if one does not match break out of the inner loop and continue with the outer loop.
- Complexity is O(m * n) m = length of desired string, n = length of string being searched

Boyer-Moore algorithm
- Bad character rule
- Worst case is O(m * n)

Knuth Morris Pratt - KMP algorithm
-Construct a longer proper prefix array (lpp)

-----------

Trie data structure (pronounced 'Try')
- Name comes from "retrieval"
- Digital tree structure
- Standard trie has one letter and each node and connects to the next C -> CO -> COP
- Standard tries and Compressed tries

-----------

Compression

- Greedy algorithm
 - Does not evaluate the entire dataset, only the current information
- Huffman algorithm
 - Build a frequency table
 - Uses variable length coding
 - Makes a binary tree using the frequency table
