import java.util.Arrays;

class BinaryTreeMain  {
	public static void main (String [] args) {
		int treeData [] = {5,3,7,6,2,1,8,4};
		Arrays.sort(treeData);

		int middleOfTree = treeData.length / 2;
		Node binaryTree = insertNode(null, treeData[middleOfTree]);		

		printNode (binaryTree);

	}

	public static Node insertNode (Node node, int data) {
		if (node == null){
			node = new Node (data);
			return node;
		}

		if (data <= node.data){
			node.leftChild = insertNode(node.leftChild, data);
		} else if (data > node.data){
			node.rightChild = insertNode(node.rightChild, data);
		}

		return node;
	}

	public static void printNode (Node n) {

		/*
			Goal is to draw something like this: (ASCII drawing)
		   1	
		  / \
		 1   3

		*/

		if (n.data != 0) {
			String parentSpacer = "   ";
			String treeLineDrawing = "  / \\\n";
			String s = String.format ("%s%d%s\n", parentSpacer, n.data, parentSpacer);
			s += treeLineDrawing;

			if (n.leftChild != null && n.rightChild != null) {
				s += String.format(" %d   %d\n", n.leftChild.data, n.rightChild.data);
			} else {
				String leftStr = "X"; String rightStr = "X";
				if (n.leftChild != null) {
					leftStr = String.format("%d", n.leftChild.data);
				}
				if (n.rightChild != null) {
					rightStr = String.format("%d", n.rightChild.data);
				}

				s += String.format(" %s   %s\n", leftStr, rightStr);				
			}

			System.out.println(s);
		}
	}

	public Node findNode (Node node, int data) {
		if (node == null)
			return null;

		if (data == node.data) {
			return node;
		}

		if (data <= node.data) {
			return findNode(node.leftChild, data);
		} else if (data > node.data) {
			return findNode(node.rightChild, data);
		}
		return null;
	}
}

class Node {
	public int data = 0;
	public Node leftChild = null;
	public Node rightChild = null;

	public Node(int data) {
		this.data = data;
	}
}
