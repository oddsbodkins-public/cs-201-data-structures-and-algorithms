import java.util.Random;
import java.util.Arrays;
import java.util.Scanner;

class TreeApplicationMain {
	public static void main (String [] args) {
		Random rand = new Random();

		int [] data = new int[10];

		for (int i = 0; i < 10; i++) {
			data[i] = rand.nextInt(500);
		}

		TreeApplication binaryTree = new TreeApplication();

		for (int i = 0; i < data.length; i++) {
			binaryTree.insert(data[i]);
		}

		System.out.println("Original array: " + Arrays.toString(data));
		binaryTree.printTree();

		Scanner scanner = new Scanner (System.in);
		System.out.println ("Type a number to search: ");
		int item = scanner.nextInt();
		Node findItem = binaryTree.find(item);
		if (findItem == null)
			System.out.println("Item Not Found");
		else
			System.out.print("Item " + findItem.data + " Found\n");

	}
}

class TreeApplication {
	Node root;

	public TreeApplication() {
		root = null;
	}

	public void printTree () {
		System.out.printf("Traversing In Order: "); 
		printInOrder(root);
		System.out.printf("\n");

		System.out.printf("Traversing Pre Order: "); 
		printPreOrder(root);
		System.out.printf("\n");

		System.out.printf("Traversing Post Order: "); 
		printPostOrder(root);
		System.out.printf("\n");
	}

	// root, left, right
	public void printPreOrder (Node node) {
		if (node == null) {
			return;
		}

		System.out.printf("%d ", node.data);
		printInOrder(node.leftChild);
		printInOrder(node.rightChild);
	}

	// left, right, root
	public void printPostOrder (Node node) {
		if (node == null) {
			return;
		}

		printInOrder(node.leftChild);
		printInOrder(node.rightChild);
		System.out.printf("%d ", node.data);
	}

	// left, root, right
	public void printInOrder (Node node) {
		if (node == null) {
			return;
		}

		printInOrder(node.leftChild);
		System.out.printf("%d ", node.data);
		printInOrder(node.rightChild);
	}

	// Returns the parent node of the leftmost node available
	public Node traverseInOrder(Node node) {
		if (node.leftChild != null) {
			if (node.leftChild.leftChild != null) {
				traverseInOrder(node.leftChild);
			}
			if (node.leftChild.leftChild == null) {
				return node;
			}
		}

		return node;
	}

	//Accessible insert method to call the recursive one
	public void insert (int data){
		root = insertNode (root, data);
	}


	// Recursive insert
	public static Node insertNode (Node node, int data) {
		if (node == null){
			node = new Node (data);
			return node;
		}

		if (data <= node.data){
			node.leftChild = insertNode(node.leftChild, data);
		} else if (data > node.data){
			node.rightChild = insertNode(node.rightChild, data);
		}

		return node;
	}

	// Accessible find method to call the recursive one
	public Node find (int data){
		Node node = findNode (root, data);
		return node;
	}

	// Recursive search
	public static Node findNode (Node node, int data) {
		if (node == null)
			return null;

		if (data == node.data) {
			return node;
		}

		if (data <= node.data) {
			return findNode(node.leftChild, data);
		} else if (data > node.data) {
			return findNode(node.rightChild, data);
		}
		return null;
	}
}

class Node {
	public int data = -1;
	public Node leftChild;
	public Node rightChild;

	public Node(int data) {
		this.data = data;
		leftChild = rightChild = null;
	}
}
