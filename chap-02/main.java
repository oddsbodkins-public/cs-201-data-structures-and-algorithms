class main {
	public static void main(String [] args) {

	}
}

class Employee {
	private double payRate;
	private String fullName;
	private double FTE;
	float hoursWorked;

	public double calculatePay() {
		return getPayRate() * getFTE() * getHoursWorked();
	}
	public double getPayRate() {
		return payRate;
	}
	public double getFTE() {
		return FTE;
	}
	public float getHoursWorked() {
		return hoursWorked;
	}
}

class UnionEmployee extends Employee {
	// everything from Employee will come over
	// new fields for UnionEmployee:

	private String barganingUnit;
	private String unionCode;
	private double unionDues;

	public void calculateUnionDues() {
	}

	private double getUnionDues() {
		return unionDues;
	}

	public double calculatePay() {
		return (super.calculatePay() - getUnionDues());
	}
}
