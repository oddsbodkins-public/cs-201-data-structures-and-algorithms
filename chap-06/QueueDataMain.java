import java.util.Queue;
import java.util.LinkedList;

class QueueDataMain {
	public static void main (String [] args) {

		Queue<String> coffeeShop = new LinkedList<>();
		coffeeShop.add("Sally");
		coffeeShop.add("Debra");
		coffeeShop.add("Jimmy");
		coffeeShop.add("Cindy");
		System.out.println(coffeeShop.peek());

		coffeeShop.remove();
		coffeeShop.remove();
		System.out.println(coffeeShop.element());

		coffeeShop.remove();
		coffeeShop.poll();
		coffeeShop.poll();

	}
}
