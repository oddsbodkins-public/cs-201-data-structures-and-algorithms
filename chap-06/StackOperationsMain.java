import java.util.NoSuchElementException;

class StackOperationsMain {
	public static void main (String [] args) {
		int plate;
		StackOperationsLinkedStack lottaPlates = new StackOperationsLinkedStack();

		//let's add some plates
		plate = 25;
		for (int i = plate; i < 150; i++) {
			lottaPlates.push(i+5);
		}
		//is the stack empty?
		if(lottaPlates.isEmpty()) {
			System.out.println("Empty Stack");
		} else {
			//peek at the top
			System.out.println("Peeking at the top = " + lottaPlates.peek());

			//show the size
			System.out.println("Size of the stack = " + lottaPlates.size());

			//pop off the top one
			System.out.println("Popping the top = " + lottaPlates.pop());
		}

		//we have 125 plates.
		// I want to remove the 50th...
		// start at the top and pop them off until you get to 50

		for(int j = lottaPlates.size(); j >= 50; j--){
			System.out.println (lottaPlates.size());
			lottaPlates.pop();
		}
	}
}


// Stack interface that lets you
// *pop, *push, *peek, *check if empty

interface StackOperationsStack {
	//add to the top
	void push(int plate);

	//remove from the top
	int pop();

	//look at first item
	int peek();

	//how many elements?
	int size();

	//is the stack empty?
	boolean isEmpty();
}

class StackOperationsNode {
	int plate;
	StackOperationsNode next;

	public StackOperationsNode(int current) {
		plate = current;
	}
}

class StackOperationsLinkedStack implements StackOperationsStack {
	//list functions below
	//in a list the head is the 'top'
	//but this is a stack so let's call it top
	private StackOperationsNode top;

	//constructor to start an empty stack
	public StackOperationsLinkedStack() {
		top = null;
	}

	public void push(int current) {
		StackOperationsNode c = new StackOperationsNode(current);
		c.next = top;
		top = c;
	}

	public int pop() {
		int returnPlate;
		returnPlate = top.plate;
		top = top.next;
		return returnPlate;
	}
	public boolean isEmpty() {
	return top == null;
	}

	public int size() {
		int counter = 0;

		for (StackOperationsNode node = top; node != null; node=node.next) {
			counter ++;
		}
		return counter;
	}

	public int peek() {
		if(top == null) {
			throw new NoSuchElementException();
		}
		return top.plate;
	}
}
